<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Resto</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="#">Data Pesan <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="menu.php">Data Menu <span class="sr-only"></span></a>
                <a class="nav-item nav-link" href="level.php">Data Level <span class="sr-only"></span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Update Data Pemesanan</h4>
<?php foreach($db->editdata($_GET['id_pesan']) as $pes) : ?>
<form action="proses.php?aksi=m_update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_pesan">ID</label>
            <input type="hidden" id="id_pesan" name="id_pesan" value="<?= $pes['id_pesan'] ?>"><br>
            <?= $pes['id_pesan'] ?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Pelanggan</label>
            <input type="text" placeholder="Masukkan Nama Pelanggan" id="nama" name="nama" class="form-control" required value="<?= $pes['nama'] ?>">
        </div>
        <div class="form-group">
            <label for="id_menu">Menu</label>
            <select class="form-control" name="id_menu">
                <option value="1"  <?= ($pes['id_menu']=="1")? "selected" : "" ?>>Mie Djoedes</option>
                <option value="2"  <?= ($pes['id_menu']=="2")? "selected" : "" ?>>Siomay</option>
                <option value="3"  <?= ($pes['id_menu']=="3")? "selected" : "" ?>>French Fries</option>
                <option value="4"  <?= ($pes['id_menu']=="4")? "selected" : "" ?>>Tea</option>
                <option value="5"  <?= ($pes['id_menu']=="5")? "selected" : "" ?>>Lemon Tea</option>
                <option value="6"  <?= ($pes['id_menu']=="6")? "selected" : "" ?>>Green Tea</option>
            </select>
        </div>
         <div class="form-group">
            <label for="id_level">Level</label>
            <select class="form-control" name="id_level">
                <option value="1"  <?= ($pes['id_level']=="1")?  "selected" : "" ?>>Level 1 : Originial</option>
                <option value="2"  <?= ($pes['id_level']=="2")?  "selected" : "" ?>>Level 2 : Pemula</option>
                <option value="3"  <?= ($pes['id_level']=="3")?  "selected" : "" ?>>Level 3 : Standar</option>
                <option value="4"  <?= ($pes['id_level']=="4")?  "selected" : "" ?>>Level 4 : Oke</option>
                <option value="5"  <?= ($pes['id_level']=="5")?  "selected" : "" ?>>Level 5 : Ekstra</option>
                <option value="6"  <?= ($pes['id_level']=="6")?  "selected" : "" ?>>Level 6 : Marah</option>
                <option value="7"  <?= ($pes['id_level']=="7")?  "selected" : "" ?>>Level 7 : Nyebelin</option>
                <option value="8"  <?= ($pes['id_level']=="8")?  "selected" : "" ?>>Level 8 : Hot</option>
                <option value="9"  <?= ($pes['id_level']=="9")?  "selected" : "" ?>>Level 9 : Cold</option>
                <option value="10" <?= ($pes['id_level']=="10")? "selected" : "" ?>>Level 0 : No Level</option>
            </select>
        </div>
        <div class="form-group">
            <label for="harga">Harga</label>
            <input type="text" placeholder="Masukkan Harga" id="harga" name="harga" class="form-control" required value="<?= $pes['harga'] ?>">
        </div>
        <div class="form-group">
            <label for="photos">Foto</label><br>
            <img src="<?= $pes['url'] ?>" width="80px" height="100px" />
            <input type="hidden" id="photos" name="photos" value="<?= $pes['photos'] ?>">
            <input type="file" class="form-control-file" id="file" name="file">
        </div>

        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="pesan.php" class="btn btn-primary">Batal</a>
        </div>
</form>

<?php endforeach ?>

</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>