<?php
    $DB_NAME = "resto";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT p.id_pesan, p.nama, m.nama_menu, l.nama_level, p.harga, concat('http://192.168.43.228/uas-2d-pemesanan-makanan-web/images/',photos) as url
		        FROM pesan p, menu m, levell l
                WHERE p.id_menu=m.id_menu and p.id_level = l.id_level";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_pesan = array();
            while($pesan = mysqli_fetch_assoc($result)){
                array_push($data_pesan, $pesan);
            }
            echo json_encode($data_pesan);
        }
    }
?>
