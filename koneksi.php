<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "resto",
        $con,
        $path = "images/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }

        public function tampildata(){
            $sql ="SELECT p.id_pesan, p.nama, p.id_menu, p.id_level, p.harga, concat('http://192.168.43.228/resto/images/',photos) as url
                FROM pesan p";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }


        public function insertdata($id_pesan,$nm,$id_menu,$id_level,$harga,$imstr,$file){
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into pesan(id_pesan, nama, id_menu, id_level, harga, photos) values(
                '$id_pesan','$nama','$id_menu', '$id_level', '$harga','$file')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $sql = "delete from pesan where id_pesan='$id_pesan'";
                    mysqli_query($this->con,$sql);
                    $hasil['respon']="gagal";
                    return $hasil;   
                }else{
                    $hasil['respon']="sukses";
                    return $hasil;
                }
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdata($id_pesan){         // fungsi mengambil data
            $sql = "SELECT p.id_pesan,p.nama,p.id_menu,p.id_level,p.harga,p.photos,concat('http://localhost/resto/images/',photos) as url
                FROM pesan p
                WHERE p.id_pesan ='$id_pesan'";  
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedata($id_pesan,$nm,$id_menu,$id_level,$hrg,$imstr,$file){ // fungsi mengudapte data
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            if($imstr==""){
                $sql = "UPDATE pesan SET nama='$nama', id_menu='$id_menu', id_level='$id_level' , harga='$harga'
                where id_pesan='$id_pesan'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //update data sukses tanpa foto
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }else{
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE pesan SET nama='$nama',id_menu='$id_menu',id_level='$id_level',harga='harga',photos='$file'
                            where id_pesan='$id_pesan'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }
        }      
        public function deletedata($id_pesan){
            $sql = "SELECT photos from pesan where id_pesan='$id_pesan'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    unlink($this->path.$photos);
                }
                $sql = "DELETE from pesan where id_pesan='$id_pesan'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }
        }
    }
?>