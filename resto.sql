/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.1.19-MariaDB : Database - resto
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`resto` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `resto`;

/*Table structure for table `levell` */

DROP TABLE IF EXISTS `levell`;

CREATE TABLE `levell` (
  `id_level` int(11) DEFAULT NULL,
  `nama_level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `levell` */

insert  into `levell`(`id_level`,`nama_level`) values 
(1,'original'),
(2,'pemula'),
(3,'standar'),
(4,'oke'),
(5,'extra'),
(6,'marah'),
(7,'nyebelin'),
(8,'hot'),
(9,'cold'),
(10,'no level');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id_menu` int(11) DEFAULT NULL,
  `nama_menu` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`id_menu`,`nama_menu`) values 
(1,'mie djoedes'),
(2,'siomay'),
(3,'french fries'),
(4,'tea'),
(5,'lemon tea'),
(6,'green tea');

/*Table structure for table `pesan` */

DROP TABLE IF EXISTS `pesan`;

CREATE TABLE `pesan` (
  `id_pesan` varchar(10) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pesan` */

insert  into `pesan`(`id_pesan`,`nama`,`id_menu`,`id_level`,`harga`,`foto`) values 
('1','ratna',1,1,11000,'mie djoedes pemula.jpg'),
('2','fernanda',2,10,10000,'siomay.jpg'),
('1','ratna',5,8,8500,'lemon tea.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
