<?php
    $DB_NAME = "resto";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";
    $data_pesan = array();
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT * FROM menu";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            while($pesan = mysqli_fetch_assoc($result)){
                array_push($data_pesan, $pesan);
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Resto</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="pesan.php">Data Pesan<span class="sr-only"></span></a>
                <a class="nav-item nav-link active" href="#">Data Menu <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="level.php">Data Level <span class="sr-only"></span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Menu</h4>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">ID</th>
        <th scope="col">Menu</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data_pesan as $pesan) : ?>
        <tr>
            <td><?= $pesan['id_menu'] ?></td>
            <td><?= $pesan['nama_menu'] ?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
