<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Resto</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="#">Data Pesan <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="menu.php">Data Menu <span class="sr-only"></span></a>
                <a class="nav-item nav-link" href="level.php">Data Level <span class="sr-only"></span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Tambah Data Pesanan</h4>
<form action="proses.php?aksi=m_insert" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_pesan">Nomor Meja</label>
            <input type="text" placeholder="Masukkan Nomor" id="id_pesan" name="id_pesan" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="nama">Nama Pelanggan</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="id_menu">Menu</label>
            <select class="form-control" name="id_menu">            
                <option value="1">Mie Djoedes</option>          
                <option value="2">Siomay</option>
                <option value="3">French Fries</option>
                <option value="4">Tea</option>
                <option value="5">Lemon Tea</option>
                <option value="6">Green Tea</option>
            </select>
        </div>
        <div class="form-group">
            <label for="id_level">Level</label>
            <select class="form-control" name="id_level">            
                <option value="1">Level 1 : Original</option>          
                <option value="2">Level 2 : Pemula</option>
                <option value="3">Level 3 : Standar</option>
                <option value="4">Level 4 : Oke</option>
                <option value="5">Level 5 : Ekstra</option>
                <option value="6">Level 6 : Marah</option>
                <option value="7">Level 7 : Nyebelin</option>
                <option value="8">Level 8 : Hot</option>
                <option value="9">Level 9 : Cold</option>
                <option value="10">Level 0 : No Level</option>
            </select>
        </div>
        <div class="form-group">
            <label for="harga">Harga</label>
            <input type="text" placeholder="Masukkan Harga" id="harga" name="harga" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="email">Foto</label>
            <input type="file" class="form-control-file" id="file" name="file">
        </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="pesan.php" class="btn btn-primary">Batal</a>
        </div>
</form>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>