<?php
    $DB_NAME = "resto";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
        $sql = "select nama_menu from menu order by nama_menu asc" ;
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_menu = array();
            while($nm_menu = mysqli_fetch_assoc($result)){
                array_push($nama_menu, $nm_menu);
            }
            echo json_encode($nama_menu);
        }
    }
?>