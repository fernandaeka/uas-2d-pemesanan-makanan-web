<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="#">Resto</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Home</a>
                <a class="nav-item nav-link" href="pesan.php">Data Pesan</span></a>
                <a class="nav-item nav-link" href="menu.php">Data Menu <span class="sr-only"></span></a>
                <a class="nav-item nav-link" href="level.php">Data Level <span class="sr-only"></span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang di Resto Mie Djoedes</h1>
            <p class="lead">Jl. Veteran No. 50A, Mojoroto, Kediri</p>
            <hr class="my-4">
            <a class="btn btn-primary btn-lg" href="about.php" role="button">Lebih Lanjut</a>
        </div>
    </div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
