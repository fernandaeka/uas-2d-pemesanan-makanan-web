<?php
    $DB_NAME = "resto";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";
    $data_pesan = array();
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT p.id_pesan, p.nama, m.nama_menu, l.nama_level, p.harga, concat('http://192.168.43.228/resto/images/',photos) as url
                FROM pesan p, menu m, levell l
                WHERE p.id_menu=m.id_menu and p.id_level = l.id_level";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            while($pesan = mysqli_fetch_assoc($result)){
                array_push($data_pesan, $pesan);
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Resto</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="#">Data Pesan <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="menu.php">Data Menu <span class="sr-only"></span></a>
                <a class="nav-item nav-link" href="level.php">Data Level <span class="sr-only"></span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Pesanan</h4>

    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "sukses ditambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "sukses diedit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "sukses dihapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "gagal ditambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "gagal diedit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "gagal dihapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data Pesan <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>

    <table class="table">
    <thead>
        <tr>
        <th scope="col">Id Pesan</th>
        <th scope="col">Nama</th>
        <th scope="col">Menu</th>
        <th scope="col">Level</th>
        <th scope="col">Harga</th>
        <th scope="col">Foto</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data_pesan as $pesan) : ?>
        <tr>
            <td><?= $pesan['id_pesan'] ?></td>
            <td><?= $pesan['nama'] ?></td>
            <td><?= $pesan['nama_menu'] ?></td>
            <td><?= $pesan['nama_level'] ?></td>
            <td><?= $pesan['harga'] ?></td>
            <td><img src="<?= $pesan['url'] ?>" width="40px" height="50px" /></td>
            <td>  <a href="update.php?id_pesan=<?php echo $pesan['id_pesan']; ?>" class="btn btn-warning">Edit</a>
                  <a href="proses.php?id_pesan=<?php echo $pesan['id_pesan']; ?>&aksi=m_delete" class="btn btn-danger">Hapus</a>
                  
            </td>

        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
<a href="insert.php" class="btn btn-success mb-3">
    Tambah Pesan
</a>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
