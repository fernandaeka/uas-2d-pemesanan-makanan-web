<?php
    $DB_NAME = "resto";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT nama_level FROM levell ORDER BY nama_level asc";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_level = array();
            while($nm_level = mysqli_fetch_assoc($result)){
                array_push($namalevel,$nm_level);
            }
            echo json_encode($namalevel);
        }
    }
?>