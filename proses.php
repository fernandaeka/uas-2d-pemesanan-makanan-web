<?php 
include 'koneksi.php';
$db = new database();

$aksi = $_GET['aksi'];
if($aksi == "m_insert"){
    $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->insertdata($_POST['id_pesan'],$_POST['nama'],$_POST['id_menu'],$_POST['id_level'],$_POST['harga'],$tempname,$filename);
    if($hasil['respon']=="sukses"){
        header("location:pesan.php?pesan=insertsuccess");
    }else{
        header("location:pesan.php?pesan=insertfailed");
    }
}elseif($aksi == "m_update"){
    $filename = $_POST['photos'];
    $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->updatedata($_POST['id_pesan'],$_POST['nama'],$_POST['id_menu'],$_POST['id_level'],$_POST['harga'],$tempname,$filename);
    if($hasil['respon']=="sukses"){
        header("location:pesan.php?pesan=updatesuccess");
    }else{
        header("location:pesan.php?pesan=updatefailed");
    }
}elseif($aksi == "m_delete"){ 	
    $hasil=$db->deletedata($_GET['id_pesan']);
    if($hasil['respon']=="sukses"){
        header("location:pesan.php?pesan=deletesuccess");
    }else{
        header("location:pesan.php?pesan=deletefailed");
    }
}
?>